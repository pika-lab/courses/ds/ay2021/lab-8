package sd.lab.ws.presentation.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import sd.lab.linda.textual.RegexTemplate;

public class RegexTemplateSerializer extends AbstractJsonSerializer<RegexTemplate> {
    @Override
    protected JsonElement toJsonElement(RegexTemplate object) {
        throw new Error("serialize object as a JsonObject");
    }
}
