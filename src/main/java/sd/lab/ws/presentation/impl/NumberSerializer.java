package sd.lab.ws.presentation.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

public class NumberSerializer extends AbstractJsonSerializer<Number> {
    @Override
    protected JsonElement toJsonElement(Number object) {
        throw new Error("serialize object as a JsonPrimitive");
    }
}
