package sd.lab.ws.presentation.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import sd.lab.ws.presentation.PresentationException;

public class NumberDeserializer extends AbstractJsonDeserializer<Number> {
    @Override
    protected Number deserializeJson(JsonElement jsonElement) {
        throw new Error("deserialize jsonElement as a JsonPrimitive containing a single number");
    }
}
