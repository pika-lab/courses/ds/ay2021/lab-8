package sd.lab.ws.presentation.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import sd.lab.ws.presentation.PresentationException;

public class StringDeserializer extends AbstractJsonDeserializer<String> {
    @Override
    protected String deserializeJson(JsonElement jsonElement) {
        throw new Error("deserialize jsonElement as a JsonPrimitive containing a single string");
    }
}
