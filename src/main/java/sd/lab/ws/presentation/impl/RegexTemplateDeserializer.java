package sd.lab.ws.presentation.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.ws.presentation.PresentationException;

public class RegexTemplateDeserializer extends AbstractJsonDeserializer<RegexTemplate> {
    @Override
    protected RegexTemplate deserializeJson(JsonElement jsonElement) {
        throw new Error("deserialize object from a JsonObject");
    }
}
