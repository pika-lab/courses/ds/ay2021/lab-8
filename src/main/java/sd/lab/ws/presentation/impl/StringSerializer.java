package sd.lab.ws.presentation.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

public class StringSerializer extends AbstractJsonSerializer<String> {
    @Override
    protected JsonElement toJsonElement(String object) {
        throw new Error("serialize object as a JsonPrimitive");
    }
}
