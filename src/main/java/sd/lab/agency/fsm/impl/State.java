package sd.lab.agency.fsm.impl;

enum State {
    CREATED, STARTED, RUNNING, PAUSED, STOPPED
}
